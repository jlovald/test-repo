var c = document.getElementById("craig_game");
var ctx = c.getContext("2d");
var row_count = 0;
var column_count = 0;
var craig_image = new Image();
craig_image.src = "media/images/placeholder.png";
var starting_index;
var canvas_w = c.clientWidth;
var canvas_h = c.clientHeight;
var render_list = new Array();
var front_index = 0;
var rect_width;
var drawing;
var cont = false;
var current_frame = null;

ctx.fillStyle = "red";
ctx.fillRect(0,0,canvas_w, canvas_h);

var frames;
function Frame(hit_location, color) {
    this.color = color;
    this.hit_location = hit_location;   // sort of like an index, no?
    this.color = color;
    this.hit = false;
}

function getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

function getRandomRow() {
    return Math.floor(Math.random() * row_count);
}

function incrementList() {
    for(var i = 1; i < frames.length; i++) {
        frames[i] = 0;
    }
}

function generateNewFrame() {
    var frame = new Frame(getRandomRow(), getRandomColor() );
    frames.push(frame);
    render_list.push(frame);
}

function drawQueue() {
   
    for(var i = 0; i < render_list.length; i++) {
        var elem = render_list[i];
        ctx.fillStyle = elem.color;
        ctx.fillRect(rect_width * i, 0, rect_width, canvas_h);
        ctx.fillStyle = "black";
        //  draw a hit location
        ctx.fillRect(rect_width * i, (canvas_h / row_count) * elem.hit_location  , rect_width, rect_width);
        ctx.drawImage(craig_image, rect_width * i, (canvas_h / row_count) * elem.hit_location  , rect_width, rect_width );
    }
}

function setupSettings(rows_in, columns_in) {
    row_count = rows_in;
    column_count = columns_in;
    starting_index = column_count / 2;
    frames = new Array(column_count);
    rect_width = canvas_w / column_count;   
    for(var i = 0; i < columns_in; i++) {
        generateNewFrame();
    }

    
}



function popFirst() {
    current_frame = render_list[0];
    if(render_list[0].hit == false) {
        clearInterval(a);
    }
    var a = new Array();
    for(var i = 1; i < column_count; i++) {
        a.push(render_list[i]);
    
        
    }
    
    //console.log(a);
    render_list = a;

    generateNewFrame();
}

function gogo() {
    console.log("Hello");
    drawing = setInterval( function() {
        //console.log("Hey");
        if(current_frame != null) {
	//current_frame.hit = true;
            if(current_frame.hit == false) {
                clearInterval(drawing);
                document.body.innerHTML = "YOu lost nerd!!";
            }
        }
	
        //  check for hit?
        //  Do logic and stuff.
        drawQueue();
        
        popFirst();
    }, 750);
}
function checkHit(x,y) {
    var spawn_heigth = (canvas_h / row_count) * current_frame.hit_location;
    console.log(render_list[0].hit_location + " asd" + " vs " + spawn_heigth);
    if(x <= rect_width && y >= spawn_heigth && y <= (spawn_heigth + rect_width)) {
        return true;
    } else {
        return false;
    }
}
document.onmouseup =  function(e) {
    var mouse_x = e.pageX;
    var mouse_y = e.pageY;
    if(cont == false) {
        cont = true;
        gogo();
    }
    if(current_frame != null) {
        if(checkHit(mouse_x,mouse_y)) {
            current_frame.hit = true;
        }
    }
    
};

function drawLoop() {
    drawQueue();
}
setupSettings(10,10);

