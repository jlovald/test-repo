var c = document.getElementById("craig_clock");
var ctx = c.getContext("2d");

var background_image = new Image();
background_image.src = "media/images/back.png";
background_image.onload = loaded;
/* CLock hand canvas and images. */
var second_c = document.createElement("canvas");
var minute_c = document.createElement("canvas");
var hour_c = document.createElement("canvas");
var hands = new Array();
var center_x = ctx.canvas.width / 2;
var center_y = ctx.canvas.height / 2;
var NUMBER_OF_IMAGES = 7;   // Static variable, need to changes this when adding images, ugly.
var image_count = 0;
var play = false;
var tick = new Audio();
tick.src = "media/audio/tick.wav";


function enableSound() {
    play = !play;
}
function go() {

    console.log(hands.length);
    for(var i = 0; i < hands.length; i++) {
        
    }

    setInterval(drawLoop, 1000);    
    //  draw initial hands.
    
}



function rotate(r_ctx, angle, hand) {
    var w = r_ctx.canvas.width;
    var h = r_ctx.canvas.height;
    r_ctx.clearRect(0,0,w, h);
    r_ctx.translate(w / 2, h / 2);
    ctx.rotate(angle);
    drawImage(hand, true);
    ctx.restore();
}

function rotatedDraw(image, draw_x, draw_y, rad) {
    ctx.save();

    ctx.translate(center_x, center_y);
    ctx.rotate(rad);    
    ctx.drawImage(image, - image.width / 2, -image.height);
    ctx.restore();
}

function rotatedDraw(image, draw_x, draw_y, rad, head) {
    ctx.save();

    ctx.translate(center_x, center_y);
    ctx.rotate(rad);    
    ctx.drawImage(image, - image.width / 2, -image.height);
    ctx.drawImage(head, - image.width / 2, -image.height, image.width, 100);
    ctx.restore();
}

function drawLoop() {
    ctx.clearRect(0,0,ctx.canvas.width, ctx.canvas.height);
    ctx.drawImage(background_image, 0, 0);
    var date = new Date();  
    var seconds = date.getSeconds();
    var minutes = date.getMinutes();
    var hours = date.getHours();
    
    if(play) {
        tick.currentTime = 0;
        tick.play();
    }
    
    hands.forEach(element => {
        //console.log(center_x)
        var draw_x = center_x - element.hand.width / 2;
        var draw_y = center_y - element.hand.height;
        //console.log(Date.now());
        //ctx.drawImage(element.hand,draw_x, draw_y);
        var angle;
        
        switch(element.rate) {
            case "s":
                angle = seconds * 2 * Math.PI / 60;
            break;
            case "h":
                angle = hours * 2 * Math.PI / 12;
            break;
            case "m":
                angle = minutes * 2 * Math.PI / 60;
            break;
        }
        console.log("Angle: " + angle / 180);
        rotatedDraw(element.hand, draw_x, draw_y, angle, element.img);
        //ctx.drawImage(element.img, draw_x, draw_y)
        
    });
}

function loaded() {
    if(++image_count == NUMBER_OF_IMAGES) {
        console.log("loaded");
        go();
    }
}

var Hand = function(c,ctx,rate, hand_src, head_src, offset_x, offset_y) {
    this.hand = new Image();
    this.rate = rate;
    this.hand.src = hand_src;
    this.img = new Image();
    this.img.src = head_src;
    this.offset_x = offset_x;
    this.offset_y = offset_y;
    hands.push(this);
    this.hand.onload = loaded;
    this.img.onload = loaded;
}


//  rotate clock hands
function rotateHands() {
    
}

function initialSetup() {
    //  could use a function here.
    var s = new Hand(second_c,second_c.getContext("2d"), "s", "media/images/s.png", "media/images/s_h.png",0,0 );
    var m = new Hand(second_c,second_c.getContext("2d"), "m", "media/images/m.png", "media/images/m_h.png",0,0 );
    var h = new Hand(second_c,second_c.getContext("2d"), "h", "media/images/h.png", "media/images/h_h.png",0,0 );
}


initialSetup();