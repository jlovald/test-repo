var c = document.getElementById("solar_system");
var ctx = c.getContext("2d");
var planets = new Array();
var drawn_frames = 0;
var sun = document.createElement("canvas");
sun.width = 200;
sun.height = 200;
var sun_context = sun.getContext("2d");
var sun_background = new Image();
sun_background.src = "media/images/sun_background.png";
var audio = new Audio();
audio.src = "media/solar.mp3";

console.log(audio);
function go() {
    console.log("asd");
    audio.currentTime = 20; 
    audio.play();
}
var craig_sun = new Image();
craig_sun.src = "media/images/placeholder.png";


var planet = function(src, angle, orbital_velocity, radius) {
    this.img = new Image();
    this.img.src = src;
    this.x = center_x + Math.cos(angle) * radius;
    this.y = center_y + Math.sin(angle) * radius;
    this.angle = angle
    this.orbital_velocity = orbital_velocity;
    this.radius = radius;
    
    // initial 
}
var time_muliplier = (3600 * 24 * 365) / 500000000;
var frame_time = 1000 / 60;

planets.push(new planet("media/images/placeholder.png", 1, 0.8, 300));
planets.push(new planet("media/images/0.png", 1.4, 0.09, 400));
planets.push(new planet("media/images/1.png", 2, 0.05, 350));
planets.push(new planet("media/images/2.png", 0.3, 0.04, 450));
planets.push(new planet("media/images/nick.jpg", 0.3, 0.04, 600));
var center_x = c.clientWidth / 2;
var center_y = c.clientHeight / 2;

var planet = function(src, angle, orbital_velocity, radius) {
    this.img = new Image();
    this.img.src = src;
    this.x = center_x + Math.cos(angle) * radius;
    this.y = center_y + Math.sin(angle) * radius;
    this.angle = angle
    this.orbital_velocity = orbital_velocity;
    this.radius = radius;
    planets.push(this);
    // initial 
}
setInterval(() => {
    ctx.clearRect(0,0, c.clientHeight, c.clientWidth);
    ctx.fillStyle = "black";
    if(++drawn_frames % 10 == 0) {
        sun_context.clearRect(0, 0, 200, 200);
        var positive = Math.random();
        if(positive > 0.5) {
            positive = -positive;
        }
        
        var rand = Math.random();
        sun_context.save();
        sun_context.translate(100, 100);
        sun_context.rotate(rand * positive * Math.PI * 2);
        
        sun_context.drawImage(sun_background,-100,-100,200, 200);
        sun_context.restore();
        
    }
    ctx.drawImage(sun, center_x - 100, center_y - 100, 200,200);
    ctx.drawImage(craig_sun, center_x - 100, center_y - 100, 200,200);
    //  Simulating :)
    for(var i = 0; i < planets.length; i++) {
        var planet_ref = planets[i];
        ctx.drawImage(planet_ref.img, planet_ref.x, planet_ref.y, 100,100);
        planet_ref.angle += planet_ref.orbital_velocity * time_muliplier;
        planet_ref.x = center_x + Math.cos(planet_ref.angle) * planet_ref.radius;
        planet_ref.y = center_y + Math.sin(planet_ref.angle) * planet_ref.radius;
    }
    
}, frame_time);