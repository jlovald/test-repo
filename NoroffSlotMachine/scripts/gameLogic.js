var c = document.getElementById("craig_game");
var ctx = c.getContext("2d");
var WIDTH = ctx.canvas.width;

var HEIGHT = ctx.canvas.height;
var START_X = 200;
var WHEEL_FRAME_OFFSET = 3;
var WHEEL_WIDTH = 300;
var WHEEL_SPACING = 10;
var WHEEL_HEIGHT = 600;
var WHEEL_IMAGE_HEIGHT = 300;
var NUMBER_OF_SPINS = 10;
var spinNumber = 0;
var NUMBER_OF_WHEELS = 3;
var NUMBER_OF_IMAGES = 4;
var IMG_PATH = "media/images/";
var a = null;
var winner;

var wheels = new Array();
var frames = new Array();
var audios = new Array();
function LoadImages() {
    for(var i = 0; i < NUMBER_OF_IMAGES; i++) {
        var img = new Image();
        img.src = IMG_PATH + i + ".png";
        var audio = new Audio();
        audio.src = IMG_PATH + i + ".mp3";
        
        audios.push(audio);
        frames.push(img);
    }
}

function GenerateWheels() {
    for(var i = 0; i < NUMBER_OF_WHEELS; i++) {
        var wheel = new SlotWheel((WHEEL_WIDTH * i) , 200, WHEEL_WIDTH);
        //  Auto adds to wheel
    }
}

function SelectTarget(wheel) {

    var winnerIndex = Math.floor(Math.random() * NUMBER_OF_IMAGES);
    Console.log(winnerIndex);
}

function DrawWheel(wheel,index) {
    wheel.ctx.clearRect(0,0,wheel.width,WHEEL_HEIGHT);
    if(index == -1) {
        wheel.prev = getRandom(wheel.prev);
    } else {
        wheel.prev = index;
    }
    
    wheel.currentImage = frames[wheel.prev];
        wheel.ctx.drawImage(wheel.currentImage,0, 0, wheel.width, WHEEL_IMAGE_HEIGHT);
    
}

let SlotWheel =  function(x, y, width) {
    this.x = x;
    this.y = y;
    this.scrollY = 0;
    this.width = width;
    this.currentImage;
    this.c = document.createElement("canvas");
    this.ctx = this.c.getContext("2d");
    this.ctx.canvas.width = this.width;
    this.ctx.canvas.height = WHEEL_HEIGHT;
    wheels.push(this);
    var prevRandom = 0;
}

function getRandom(prev) {
    var a = prev;
    while(a == prev) {
        a = Math.floor(Math.random() * NUMBER_OF_IMAGES);
    }
    return a;
}

function initialSetup() {
    LoadImages();
    //LoadImages()
    GenerateWheels();
    /*
    setTimeout( function () {
        
        setInterval(function() {

            ctx.clearRect(0, 0, WIDTH, HEIGHT);
            //console.log("yo");
            for(var i = 0; i < wheels.length; i++) {
                var element = wheels[i];
                DrawWheel(element);
                ctx.drawImage(element.c,0, 100);
                //console.log("Hello");
            }
            
        }, 1000 / 60);
    }, 1000);
    */
    // testing
    ctx.font = " 40px Consolas ";
    ctx.fillStyle = "red";
    ctx.fillText("Click on the canvas to spin!",0,40);
    
}

function Draw() {
    ctx.clearRect(0,0,HEIGHT, WIDTH);
    if(spinNumber == NUMBER_OF_SPINS-1) {
        wheels.forEach( e => {
            DrawWheel(e,winner);
            ctx.drawImage(e.c,e.x,0,300,300);
        });
    } else {
        wheels.forEach( e => {
            DrawWheel(e,-1);
            ctx.drawImage(e.c,e.x,0,300,300);
        });
    }
    
   
    if(++spinNumber >= NUMBER_OF_SPINS) {
        clearInterval(a);
        audios[winner].currentTime = 3;
        audios[winner].play();
    }

    //  check for win
}
function Test() {
    if(a == null) {
        winner = Math.floor(Math.random() * NUMBER_OF_IMAGES);
        a = setInterval(Draw, 400);
    }
    //a = setInterval(Draw, 400);
}
var a;


initialSetup();

