console.log("Hello world");
var beats_per_measure = 4;
var measures = 4;


var arr = new Array();
var row = function(audio_file, id, measures, beats, craig_version) {
    this.audio = new Audio();
    this.audio.src = audio_file;
    var row_element = document.getElementById(id);
    this.beats = new Array();
    
    //console.log(img.src);
    
    for(var i = 0; i < beats_per_measure * measures; i++) {
        var td = document.createElement("td");
        td.onclick = toggle_beat;
        var img = document.createElement("img");
        img.src = "media/" + craig_version  + ".png";
        img.width = 80;
        img.height = 80;
        td.appendChild(img);
        row_element.appendChild(td);
        td.setAttribute("toggled","false"); 
        td.toggled = "false";
        console.log(td.toggled);
        this.beats.push(td);
        td.style = "background-color: white";
    }
}

arr.push(new row("media/audio/clap.wav", "tr_0",4,4,0));
arr.push(new row("media/audio/kick.wav","tr_1",4,4,1));
arr.push(new row("media/audio/snare.wav","tr_2",4,4,2));
arr.push(new row("media/audio/crash.wav","tr_3",4,4,3   ))
//arr[0].audio.play();

function play(beat_delay) {
    setInterval(function() {
        var rows = document.getElementsByTagName("tr");
        var previousBeat = currentBeat - 1;
        if(currentBeat == 0) {
            previousBeat = beats_per_measure * measures - 1;
        }
        if(currentBeat == beats_per_measure * measures) {currentBeat = 0};
        for(var i = 0; i < rows.length; i++) {
            var curr = arr[i].beats[currentBeat];
            var prev = arr[i].beats[previousBeat];
            curr.style = "background-color: gold";
            if(prev.toggled == "true") {
                prev.style = "background-color: green";
            } else {
                prev.style = "background-color: white";
            }
            if(arr[i].beats[currentBeat].toggled == "true") {
                arr[i].audio.currentTime = 0;
                arr[i].audio.play(); 
            }
        }
        currentBeat++;
    },beat_delay);
}
var currentBeat = 0;

function toggle_beat(e) {
    
    console.log(e.target.parentElement.toggled);
    var frame = e.target.parentElement;
    
    if(frame.toggled == "false") {
        frame.toggled = "true";
        frame.class = "toggled";
        frame.style = "background-color: green";
    } else {
        frame.toggled = "false";
        frame.class = "untoggled";
        frame.style = "background-color: white";
    }
    console.log(frame.toggled);
    console.log("reeeee: " + frame.style);
   // e.setAttribute("toggled,")
}
var media = "media/"
var audio_folder = media + "    audio/"
var sound_files = [audio_folder + 'clap.wav', audio_folder + 'crash.wav', audio_folder + 'kick.wav', audio_folder + 'snare.wav'];
var audios = [sound_files.length];
for(var i = 0; i < sound_files.length;i++) {
    audios[i] = new Audio();
    audios[i].src = audios[i];
    audios[i].onload = function(e) {
        e.play();
    };
}