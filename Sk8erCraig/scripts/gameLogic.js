var c = document.getElementById("craig_game");
var ctx = c.getContext("2d");
var audio = new Audio();
audio.src = "media/audio/gogo.mp3";
var states = new Array();
var obstacles = new Array();
var NUMBER_OF_OBSTACLES = 4;

var craig_skate = new Image();
craig_skate.src = "media/images/flat/craig2.png";
var obstacle_images = new Array();


for(var i = 0; i < NUMBER_OF_OBSTACLES; i++) {
    var tmp_img = new Image();
    tmp_img.src = "media/images/obstacles/" + i + ".png";
    obstacle_images.push(tmp_img);
}

var Obstacle = function(img, width, height, x ,y ) {
    this.img = img;
    this.width = width;
    this.height = height;
    this.x = x;
    this.y = y;
    obstacles.push(this);
}

var PlayerState = function(img_src_array) {
    this.ani_frames = new Array();
    img_src_array.forEach(element => {
        var img = new Image();
        img.src = element;
        this.ani_frames.push(img);
    });
    states.push(this);
}

var player = function(states_array) {
    this.gravity = 5;
    
    this.states = states_array;
    this.c = document.createElement("canvas");
    this.ctx = this.c.getContext("2d");
    this.current_frame = 0;
    this.x = 0;
    this.y = 0;
    this.y_vel = 0;
    this.frame_count = this.states[0].ani_frames.length;
    this.width = 0;
    this.height = 0;
    //  Needs to be resized.
}

function setPlayerCanvasHeight(player) {
    var state = player.states[0];
    ////  console.log(state);
    player.height =  craig_skate.height + player.states[0].ani_frames[0].height;
    
    player.width = state.ani_frames[0].width;
    player.ctx.canvas.width = player.width;
    player.ctx.canvas.height = player.height;
    ////  console.log(player.height);
}
var strs = new Array();
for(var i= 0 ; i < 3; i++) {
    strs.push("media/images/flat/board" + i + ".png");
}

var ps = new PlayerState(strs)
var tmp_states = new Array(ps);
var p = new player(tmp_states);

function alignSkateboard() {
    if(p.y + p.height + p.y_vel> ctx.canvas.height) {
        p.y = ctx.canvas.height - p.height;
        console.log("Align!");
    } else {
        p.y += p.y_vel;
        p.y_vel += p.gravity;
    }
}

function jump() {
    if(audio.currentTime == 0) {
        audio.currentTime = 20;
        audio.play();
    }
    if(p.y >= ctx.canvas.height - p.height - 10) {
        p.y_vel = -80;
    }
   
}

function drawStateFrame(p) {
    
    var current_frame = p.current_frame++;
    checkOut();
    ctx.clearRect(0,0, ctx.canvas.width, ctx.canvas.height);
    p.ctx.clearRect(0,0, p.states[0].width, p.states[0].height);
    drawObstacles();
    var skateboard = p.states[0].ani_frames[current_frame % p.frame_count]; 
    p.ctx.drawImage(craig_skate,0,skateboard.height );
    ////  console.log(p.states[0].ani_frames);
    
    ////  console.log("Hey: " + (craig_skate.height + skateboard.height));
    p.ctx.drawImage(skateboard , 0, craig_skate.height);
    //  console.log(p.width + " " + p.height);
    //  console.log(p.height);
   // p.ctx.fillStyle = "black";
    //p.ctx.fillRect(0,0, p.width, p.ctx.canvas.height);
    alignSkateboard();
    ctx.drawImage(p.c, 0, p.y, p.width, p.height);
    alignSkateboard();
}

function drawObstacles() {
    obstacles.forEach(element => {
        
        ctx.drawImage(element.img, element.x, ctx.canvas.height - element.height);
        scrollCanvas(element, 50);
    });
    
}

function checkOut() {
    if(obstacles.length == 0) return;
    //  check edge
    if(obstacles[0].x + obstacles[0].width < 0) {
        tmp_array = new Array();
        obstacles.forEach(element => {
            if(element != obstacles[0]) tmp_array.push(element);
        });
        obstacles = tmp_array;
        console.log("go");
        createNewObstacle(ctx.canvas.width + 750);
    }

}

function createNewObstacle(x_value) 
{

    var x = x_value;
    if(x_value < 0) x = (canvas.width + 300 * Math.floor(Math.random));

    var randomIndex = Math.floor(Math.random() * obstacle_images.length);
    var image = obstacle_images[randomIndex];
    var obs = new Obstacle(image,image.width, image.height,x, ctx.canvas.height - image.height);
    
} 

function scrollCanvas(obstacle, speed) {
    obstacle.x -= speed;
}

function overridePlayerHeight(player, width, height) {
    player.width = width;
    player.height = height;
}



setTimeout(function () {
    //  Placeholder start.
    
    setPlayerCanvasHeight(p);
    var cnt = 1;
    
    createNewObstacle(c.width + (700 * cnt++));
    createNewObstacle(c.width + (700 * cnt++) + 400);
    createNewObstacle(c.width + (700 * cnt++) + 750);
    console.log(obstacles);
    overridePlayerHeight(p, 300, 300);
    p.y = ctx.canvas.height - p.height;
    //  console.log("Done");
    setInterval(drawLoop, 50);
    //drawStateFrame(p);
}, 1000);
function drawLoop() {
    drawStateFrame(p);
}