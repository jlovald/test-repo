var c = document.getElementById("craig_game");
var ctx = c.getContext("2d");
var c2 = document.createElement("canvas");
var ctx2 = c2.getContext("2d");
c2.width = ctx.canvas.width;
c2.height = ctx.canvas.height;
var audio = new Audio("chopin.mp3");
var transparency = 0;
var image = new Image();
image.src = "dew.png";
var img_x = 0;
var norge = new Image();
var sverige = new Image();
norge.src = "norge.png";
sverige.src = "sweden.png";
function run() {
    setInterval(function() {
        ctx.clearRect( 0,0, 2000, 2000);
        ctx2.clearRect(0,0,2000,2000);
        ctx.drawImage(norge, 1800 ,0 ,200, 200)
        ctx.drawImage(image, img_x,0);
        ctx2.fillStyle = "grey";
        ctx2.fillRect(0,0,2000,2000);
        ctx2.drawImage(sverige, 0,0,200,200);
        ctx2.font = "60px Arial";
        ctx2.fillStyle = "Black";
        ctx2.globalAlpha = transparency;
        ctx2.fillText("We miss you :(",500,500);
        ctx2.fillText("Gone, but not forgotten.", img_x + image.width, 300);
        ctx2.font = "40px Arial";
        ctx2.fillText("Experis Kull 4 2020-2020.",500,550);
        ctx.drawImage(c2, 0,0);
        transparency += 0.002;
        img_x -= 1.7;
    }, 50);
    audio.play();
}
