var c = document.getElementById("craig_game");
var ctx = c.getContext("2d");
var points = 0;
var running = false;
var draw;
var scroller_speed = -3;   //  10 px per frame.
var grav_constant = 0.2;
var min_obj_distance = 200;
var objects = new Array();
var min_distance = 200;
var multiplier = 2  ;
var player; // player object
var max_obstacles = 6;  //  multiple of 2;
//  add input
var frame_delay = 0;
var player_vel_y = 0;
var player_sound = new Audio();
player_sound.src = "media/flap.wav";
var up_pipe = new Image();
var down_pipe = new Image();

up_pipe.src = "media/up.png";
down_pipe.src = "media/down.png";
back_0 = new Image();
back_0.src = "media/background_0.jpg";
back_1 = new Image();
back_1.src = "media/background_1.jpg";



var backgrounds = new Array();
var background_image = function(img,x,y) {
    this.img = img;
    this.x = x;
    this.y = y;
    backgrounds.push(this);
}
new background_image(back_0, 0, 0);
new background_image(back_1, 1300, 0);  //  fuck it 



function drawBackground() {
    backgrounds.forEach(e => {
        ctx.drawImage(e.img, e.x, e.y);
    });
}

function hitDetection(a) {
    var corner =  function(x,y) {
        this.x = x;
        this.y = y;
    };
    var corners = new Array();
    corners.push(new corner(a.x, a.y)); //    upper left.
    corners.push(new corner(a.x + a.width, a.y));
    corners.push(new corner(a.x, a.y + a.height));
    corners.push(new corner(a.x + a.width, a.y + a.height));
    var hit = false;
    //  could always check the first element to reduce complexity.
    objects.forEach(element => {
        if(element != a) {
            if(a.x > element.x + element.width && !element.passed) {

                element.passed  = true;
                points++;
            }
            //  check all corners.
            corners.forEach(elem =>  {
                var x = elem.x;
                var y = elem.y;
                
                if(x >= element.x && x <= element.x + element.width
                     && y >= element.y && y <= element.y + element.height) {
                         console.log("HIT");
                        hit = true;
                        return;
                } 
            });
            if(hit) return;
        }
    });
    if(hit) {
        return true;
    }
    if(player.y < 0) {
        return true;
    } else if(player.y + player.height >= 700) return true;
    
    return false;
}

var animation = function() {
    this.frames = new Array();
    function addCommand(command) {
        this.frames.push(command);
    }
}

var Object =  function(img_src, img, width, height, animations, x, y) {

    if(img == null) {
        this.img = new Image();
        this.img.src = img_src;
    } else {
        this.img = img;
    }
    this.passed = false;
    this.x = x;
    this.y = y;
    this.height = height;
    this.width = width;
    this.animations = null;
    objects.push(this);

};

function moveObjects() {
    objects.forEach(element => {
        if(element != player) {
            element.x += scroller_speed;
        }
    });
    //  player movement.
    player.y += player_vel_y;
    player_vel_y += grav_constant;
    backgrounds.forEach(element => {
        element.x += scroller_speed;
        if(element.x + 1300 < 0) { //  stupid
            element.x += 2580;
        }
    });
    
}

function jump() {
    //console.log("fug");
    if(!running) {
        initialSetup();

        running = true;
        draw = setInterval(drawLoop, 1000 / 60);
    } else {
        playerJump();
    }
}
function playerJump() {
    console.log(frame_delay);
    if(frame_delay <= 0) {
        frame_delay = 20;
        player_vel_y -= 10;
        player_sound.currentTime = 0.15;
        player_sound.play();
    }
    //  We can bake this into the animation :)
    
}

function detectOutOfBounds() {
    var array = new Array();
    objects.forEach(element => {
        if(element.x + element.width < 0) {
            array.push(element);
        }
    });
    return array;
}

function createObstacle(x) {
    var spawn_location = Math.floor(Math.random());
    var pipe_height = 200;

    var offset = Math.floor(300 * Math.random());
    if(Math.random > 0.5) {
        offset = -offset;
    }
    pipe_height += offset;

    var pipe_distance = Math.floor(min_distance + Math.random() * 10);
    if(spawn_location < 5) {
        let object_0 = new Object(null, down_pipe, 100, pipe_height, null, x, 0);
        let object_1 = new Object(null, up_pipe, 100, 1000 - (pipe_height + pipe_distance),null, x, pipe_height + pipe_distance);
        
        //objects.push(;
    } else {

    }

}
//  unused, stupid
function removeObstacle() {
    if(objects.length > 1) {
        objects.pop();
    }
}

function drawLoop() {
    ctx.clearRect(0,0, 2000,2000);
    
    backgrounds.forEach(element => {
       ctx.drawImage(element.img, element.x, element.y); 
    });

    var out = detectOutOfBounds();
    if (out.length > 0) {
        var tmp = new Array();
        tmp.push(player);
        objects.forEach(element => {
            if(element != player) {
                if(!out.includes(element)) {
                    tmp.push(element);
                }
            }
        });
        
        objects = tmp;

        createObstacle(1300);
    }
    objects.forEach(e => {
        //console.log(e);
        //clearInterval(draw);
        ctx.drawImage(e.img, e.x,e.y, e.width,e.height);    
    });
    ctx.font = "30px georiga";
    ctx.fillText("Points: " + points / 2, 500, 200, 200);
    var hit = hitDetection(player);
    if(hit) {
        console.log("Game over nerd.");
        clearInterval(draw);
    }
    frame_delay--;
    moveObjects();
}
//  hard code this.
function initialSetup() {
        createObstacle(1300);
        createObstacle(1800);
        createObstacle(2300);
        player = new Object("media/images/placeholder.png", null, 50, 50, null, 100,300);
}
