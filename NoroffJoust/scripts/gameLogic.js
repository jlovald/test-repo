var c = document.getElementById("craig_game");
var ctx = c.getContext("2d");
var y_accel_per_frame = 0.25;
var rounds = 5;
var currentRound = 0;
var canvas_height = ctx.canvas.height = window.innerHeight;
var canvas_width = ctx.canvas.width = window.innerWidth;
var player_width = 300;
var player_height = 200;
var x_vel = 5;
var JUMP_DELAY = 5;
var JUMP_SPEED = 8;
var menu = true;
let characters = new Array();
var audio = new Audio();
audio.src = "media/audio/ncruise.mp3";


let Character = function(src, width, height, orientation, initial_x, initial_y) {
    this.c = document.createElement("canvas");
    this.ctx = this.c.getContext("2d");
    this.img = new Image();
    this.img.src = src;
    this.width = (this.ctx.canvas.width = width);
    this.height = (this.ctx.canvas.height = height);
    this.orientation = orientation;
    var tmp = this;
    this.initial_x = initial_x;
    this.initial_y = initial_y;
    this.x = initial_x;
    this.y = initial_y;
    this.current_delay = -1;
    this.img.onload = function () {
        tmp.ctx.drawImage(tmp.img, 0, 0, tmp.width, tmp.height);
    }

    characters.push(this);
    this.y_vel = 0;
    this.points = 0;
    
    //  Should handle onload
}

function TranslateCharacters(chars) {
    chars.forEach(e => {
        e.x += e.orientation * x_vel;
        e.y += e.y_vel;
        e.y_vel += y_accel_per_frame;
    }); 
}

function OutOfBounds(chars) {
    let ret = new Array();
    chars.forEach(e => {
        if(e.y < 0) {
            ret.push(e);

        }  else if(e.y + e.height >= canvas_height){
            ret.push(e);
        }
    });
    return ret;
}

function DetectWinner() {
    //  assumes two characters.
    if(characters[0].x > characters[1].x) {
        if(characters[0].y > characters[1].y) {
            return characters[0];
        } else if(characters[1].y > characters[0].y){
            return characters[1];
        } else {
            //  dirty.
            return characters;
        }
    }

}

function DrawChars(chars) {
    chars.forEach(e => {
        ctx.drawImage(e.c, e.x, e.y, e.width, e.height);
    });
}

function Reset(characters) {
    characters.forEach(e => {
        e.x = e.initial_x;
        e.y = e.initial_y;
        e.y_vel = 0;
    });
}

function Restart() {

}

function Jump(chararcter) {
   // console.log(chararcter)
    if(chararcter.current_delay <= 0 && draw_interval != null) {
        chararcter.y_vel -= JUMP_SPEED;
        chararcter.current_delay = JUMP_DELAY;
    }
    
}

function DrawScore() {
    ctx.font = "40px consolas";
    ctx.fillText("Team Craig points: " + characters[0].points,0,100);
    ctx.fillText("Team Dewalt points: " + characters[1].points, canvas_width / 2, 100);
}

function DecrementDelay(chars) 
{
    chars.forEach(e => {
        e.current_delay--;
    });
}
function ClearCanvas() {
    ctx.clearRect(0,0,canvas_width,canvas_height);
}

function Menu() {
    ClearCanvas();
    ctx.font = "40px consolas";
    ctx.fillText("Player one jump with W", 0,50);
    ctx.fillText("Player two jump with UP", canvas_width / 2, 50);
    ctx.fillText("Press jump to start.\n",canvas_width/4, 100);
    ctx.fillText("Click on the canvas to vibe\n", canvas_width/ 4, 150);
}

function Vibe() {
    audio.play();
    audio.onended = function() {
        audio.currentTime = 0;
        audio.play();
    }
}

function RoundOver() {
    clearInterval(draw_interval);
        draw_interval = null;
        Reset(characters);
        count_down = setInterval( function() {
            if(count_down_seconds > 0) {
                ClearCanvas();
                DrawScore();
                ctx.font = "70px Georgia";
                ctx.fillStyle = "black";
                ctx.fillText("Restarting in:\n" + count_down_seconds--, 0, 50);
            } else {
                clearInterval(count_down);
                count_down_seconds = 5;
                draw_interval = setInterval(DrawLoop, 1000 / 60);
                //
            }
            
        }, 1000)
        return;
}

function CheckIntersection(c) {
    var craig = c[0];
    var dewalt = c[1];
    
    if(craig.x + craig.width >= dewalt.x && craig.x <= dewalt.x +dewalt.width) {
        console.log()
        //  Inside x-axis.
        if((craig.y >= dewalt.y && craig.y <= dewalt.y + dewalt.height) || dewalt.y >= craig.y && dewalt.y <= craig.y + craig.height) {
            if(craig.y < dewalt.y) {
                return craig;
            } else if(dewalt.y < craig.y){
                return dewalt;
            } else {
                return null;
            }
        }
    }
}

function DrawLoop() {
    ctx.clearRect(0,0,canvas_width,canvas_height);
    DrawScore();
    TranslateCharacters(characters);
    DrawChars(characters);
    DecrementDelay(characters);
    
    if(CheckBorder(characters)) {
        RoundOver();
        return;
    }
    var out = OutOfBounds(characters);
    if(out.length == 2) {
        RoundOver();
        return;
    } else if(out.length == 1){
        out[0].points--;
        currentRound++;
        if(currentRound == rounds) {
            
            clearInterval()
        } else {
            RoundOver();
            return;
        }
    }
    var winner = CheckIntersection(characters);
    if(winner == null) {
        
    } else {
        console.log(winner);
        winner.points++;
        currentRound++;
        if(currentRound == rounds) {
            clearInterval(draw_interval);
        } else {

            RoundOver();
        }
        
        
    }
    
    
}

function Bufix() {
    if(draw_interval == null) {
        draw_interval = setInterval(DrawLoop, 1000 / 60);
    }
    
}

var count_down_seconds = 5;
var count_down;
var draw_interval;
var initialize = function() {
    var a = new Character("media/images/p1.png", player_width, player_height, 1, 1, canvas_height / 2);
    a = new Character("media/images/p2.png", player_width, player_height, -1, canvas_width - player_height, canvas_height / 2);
    Run();
}

function CheckBorder(chars) {
    var ret = false;
    chars.forEach(e => {
        if(e.orientation > 0) {
            console.log(e.width + e.x > canvas_width);
            if(e.player_width + e.x > canvas_width) {
                ret =  true;
            }
        } else {
            if(e.x < 0) {
                ret = true;
            }
        }
    });
    return ret;
}

document.addEventListener("keydown", e => {
    console.log(e.keyCode);
    if(e.keyCode === 87) {
        if(menu) {
            menu = false;
            draw_interval  = setInterval(DrawLoop, 1000 / 60);
            return;
        }
        Jump(characters[0]);
    } else if(e.keyCode === 38) {
        if(menu) {
            menu = false;
            draw_interval  = setInterval(DrawLoop, 1000 / 60);
            return;
        }
        Jump(characters[1]);
    }
});

function Run() {
    Menu();
    
    
}
initialize();

// TODO: test win, maybe force new round?, add controls, add images.
//  Menu, reset, sound.