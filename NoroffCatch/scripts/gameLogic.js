var c = document.getElementById("craig_game");
var ctx = c.getContext("2d");
var PLAYER_HEIGHT = 200;
var PLAYER_WIDTH = 100;

var player = null;
var WIDTH = ctx.canvas.width;
var HEIGHT = ctx.canvas.height;
var NUMBER_OF_BACKGROUND_IMAGES = 3;
var NUMBER_OF_DROPPING_IMAGES = 3;
var NUMBER_OF_CHARACTERS = 4;
var NUMBER_OF_BACKGROUND_SONGS = 4;
var loadedCharacters = 0;
var currentLevelIndex = -1;
var currentLevel = null;
var level_count = 0;
var droppingImages = new Array();
var backgroundImages = new Array();
var characterAudios = new Array();
var backgroundAudios = new Array();
var music = new Array();
let BACKGROUND_PATH = "media/images/backgrounds/";
let DROPPING_PATH = "media/images/dropping/";
let CHARACTER_PATH = "media/images/characters/";
let AUDIO_PATH = "media/audio/";
let ANIMATION_PATH = "media/images/animation/";
let NUMBER_OF_FRAMES = 6;
let defaultAnimation = new Array();
let playerSounds = new Array();
var players = new Array();

var DROPPING_SIDE_LENGTH = 20; // implied square.
var interval;
var player;
var playerImages = new Array();

for(var i = 0; i < NUMBER_OF_BACKGROUND_IMAGES; i++) {
    let img = new Image();
    img.src = BACKGROUND_PATH + i + ".png";
    backgroundImages.push(img);
}
for(var i = 0; i < NUMBER_OF_DROPPING_IMAGES; i++) {
    let img = new Image();
    img.src = DROPPING_PATH + i + ".png";
    droppingImages.push(img);
}

let Player = function(img, width, height, ani_frames, ani_delay, in_audio, hit_audio, miss_audio) {
    this.img = img;
    this.c = document.createElement("canvas");
    this.ctx = this.c.getContext("2d");
    this.ani_frames = ani_frames;
    this.ani_delay = ani_delay;
    this.ani_curr_frame = 0;
    this.x = 0;
    this.ani_countdown = 0; // USE FOR CHARACTER.
    this.y = HEIGHT - img.height;
    this.width = width;
    this.height = height;
    this.ctx.canvas.width = width;
    this.ctx.canvas.height = height;
    this.in_audio = in_audio;
    this.hit_audio = hit_audio;
    this.miss_audio = miss_audio;
    players.push(this);
}
for(var i = 0; i < NUMBER_OF_CHARACTERS; i++) {
    var img = new Image();
    img.src = CHARACTER_PATH + i + ".png";
    img.onload = function() {
        if(++loadedCharacters == NUMBER_OF_CHARACTERS) {
            PopulateDiv();
        }
    }
    
    var a = new Audio(CHARACTER_PATH + i + ".mp3");
    a.volume = 0.7;
    playerSounds.push(a);
    playerImages.push(img);
    new Player(playerImages[i], PLAYER_HEIGHT, PLAYER_WIDTH,
        defaultAnimation, 4, null, playerSounds[i], null);
}
for(var i = 0; i < NUMBER_OF_BACKGROUND_SONGS; i++) {
    backgroundAudios.push( new Audio(AUDIO_PATH + "background/b" + i+".mp3"));

}
for(var i = 0; i < NUMBER_OF_FRAMES; i++) {
    var img = new Image();
    var s = i + 1;
    var t = "" +s;
    img.src = ANIMATION_PATH + t + ".png";
    
    defaultAnimation.push(img);
}

function PopulateDiv() {
    for(var i = 0; i < NUMBER_OF_CHARACTERS; i++) {
        var a = document.createElement("img");
        a.width = 50;
        a.height = 50;
        a.src = CHARACTER_PATH + i + ".png";
        a.id = i;
        a.onclick = function(s) {
            console.log(s.target);
            console.log(s.target.id)
            player = players[s.target.id];
        }
        document.getElementById("characterSelect").appendChild(a);
    }
}



function randomIndex() {
    return Math.floor(Math.random() * NUMBER_OF_DROPPING_IMAGES);
}

var levels = new Array();
let Level = function(background, amount, gravSpeed, y, maxFalling,
     playbackSpeed, cooldown, backgroundMusic, speed_seed) {
    this.c = document.createElement("canvas");
    this.ctx = this.c.getContext("2d");
    this.ctx.canvas.width = WIDTH;
    this.ctx.canvas.height   = HEIGHT;
    this.y = y;
    this.cooldown = cooldown;
    this.initial_cooldown = cooldown;
    this.playbackSpeed = playbackSpeed;
    this.maxFalling = maxFalling;
    this.stored = new Array();
    this.falling = new Array();
    this.audio = backgroundMusic;
    this.gravSpeed = gravSpeed;
    this.amount = amount;
    this.background = background;
    
    this.defaultDropDelay = cooldown;
    this.dropDelay = cooldown;
    var seed = speed_seed;
    
    let a = this.stored;
    this.x_vel_roof = 30;
    for(var i = 0; i < amount; i++) {
        
        a.push(new DropImage(randomIndex() , DROPPING_SIDE_LENGTH,
        DROPPING_SIDE_LENGTH,GetRandomXCoordinate(), 0,GetRandomPlusMinus() * GetRandomSpeed(seed),i));
    }
    level_count++;
    levels.push(this);
}

function moveTo(level, amountToMove, giveXVel) {
    for(var i = 0; i < amountToMove; i++) {
        if(level.stored.length == 0) {
            return;
        } else {
            level.stored.push(level.falling.shift());
        }
    }
}

//  Within bounds
function GetRandomXCoordinate() {
    return Math.floor(Math.random() * (WIDTH - DROPPING_SIDE_LENGTH));
}

function GetRandomSpeed(seed) {
    return Math.floor(Math.random() * seed);
}

function GetRandomPlusMinus() {
    if(Math.random() > 0.5) {
        return -1;
    } else {
        return 1;
    }
    
}
//  Todo audio.
let DropImage = function(img_index, width, height,x,y, x_vel, id) {
    //console.log(droppingImages[img_index] + " " + img_index);
    this.img = droppingImages[img_index];
    this.width = width;
    this.height = height;
    this.x = x; 
    this.y = y;
    this.x_vel = x_vel;
    this.y_vel = 0;
    this.id = id;
}




//  detects hitting character

function RemoveElement(level, elem) {
    var newFalling = new Array();
    level.falling.forEach(element => {
        if(element.id != elem) {
            
            newFalling.push(element);
            
        }
    });
    console.log(level.falling.length + "  vs   " + newFalling.length)
    return newFalling;
}

function DetectHit() {
    var array = currentLevel.falling;
    
    for(var i = 0; i < array.length; i++) {
        
        if(array[i].y + array[i].height < player.y) {
            break;
        } else {
            var left_x = array[i].x;
            var right_x = array[i].x + array[i].width;
            //  assumes that the object is smaller than the player.
            if((right_x >= player.x && right_x <= player.x + player.width) || left_x >= player.x && left_x <= player.x + player.width) {
                //var a =
                
                currentLevel.falling =  RemoveElement(currentLevel, array[i]);
                player.hit_audio.play();
                return;
            }
        }   
    }
   
}

function DecreaseHealth() {

}

function checkFallingOut(level) {
    var falling = level.falling;
    if( falling.length > 0 && falling[0].y + falling[0].height > HEIGHT) {
        //  Out of bounds.
        //
        level.shift();
        DecreaseHealth();
    }
    
    return;
}

function GenerateLevel(background, amount, gravSpeed, y, maxFalling, playbackSpeed, initial_cooldown, backgroundMusic, speed_seed) {
    new Level(background, amount, gravSpeed, y, maxFalling, playbackSpeed, initial_cooldown, backgroundMusic, speed_seed);
}

function Testing() {
    GenerateLevel(backgroundImages[0],20,0.2,0,5,1,50,backgroundAudios[1], 5);
    GenerateLevel(backgroundImages[1], 30, 0.3, 0, 10, 0, 30, backgroundAudios[0],30);
    backgroundAudios[3].currentTime = 16;
    GenerateLevel(backgroundImages[2], 500, 0.4, 0, 10, 0, 1, backgroundAudios[3],80);
    currentLevelIndex++;
    StartLevel();
    //interval = setInterval(DrawLoop, 1000 / 60);
}
function ClearCanvas(ctx) {
    ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
}

function DrawPlayer(player) {
    if(player.ani_countdown-- <= 0) {
        player.ani_countdown = player.ani_delay;
        player.ani_curr_frame++;
        ClearCanvas(player.ctx);
    }
     if(player.ani_curr_frame == player.ani_frames.length) {
        player.ani_curr_frame = 0;    
    }
    
    player.ctx.drawImage(player.ani_frames[player.ani_curr_frame],0,0,player.width,player.height);
    player.ctx.drawImage(player.img,player.width / 2 - 25,0,50,50);


}

function DrawLevel(level) {
    ClearCanvas(level.ctx);
    level.ctx.drawImage(level.background,0,0, HEIGHT, WIDTH);
    level.falling.forEach(element => {
        //  console.log(element.y);
        level.ctx.drawImage(element.img,element.x, element.y,40, 40);
    });
}

function MovePlayer(x) {
    if(player != null) {
        player.x = x;
    }
}

function ApplyTranslation(level) {
    level.falling.forEach( e => {
        //console.log(e);
        e.x += e.x_vel;
        e.y += e.y_vel;
        e.y_vel += level.gravSpeed;
    });
}
//  level complete
function CheckEmpty(currentLevel) {
    if(currentLevel.stored.length == 0 && currentLevel.falling.length == 0) {
        return true;
    } else {
        return false;
    }
}
function DetectBottom(currentLevel) {
    if(currentLevel.falling.length != 0) {
        let lowest = currentLevel.falling[0];
        if(lowest.y > HEIGHT) {
            currentLevel.falling.shift();
            //  Play sound?
            DecreaseHealth();
        }
    }
}

function CheckWallHit(currentLevel) {
    currentLevel.falling.forEach(e => {
        if(e.x <= 0 || e.x + DROPPING_SIDE_LENGTH >= WIDTH) {
            e.x_vel *= -1;
        }
    });
}
function moveToFalling(level) {
    //console.log("Falling");
    if(--level.cooldown <= 0 && level.stored.length > 0) {
        level.cooldown = level.initial_cooldown;
        level.falling.push(level.stored.shift());
    }
}

document.addEventListener("mousemove", function(e) {
    MovePlayer(e.x);
});

function StartLevel() {
    if(currentLevelIndex == 0) {
        currentLevel = levels[currentLevelIndex];
        currentLevel.audio.play();
        //  Set speed?
        interval = setInterval(DrawLoop, 1000 / 60);
    }else {
        if(currentLevelIndex == levels.length) {
            clearInterval(interval);
            return;
        } else {
            if(currentLevel.audio == levels[currentLevelIndex].audio) {
                //  Change playback rate?
            } else {
                levels[currentLevelIndex-1].audio.pause();
                levels[currentLevelIndex-1].audio.currentTime = 0;
                
            }
            clearInterval(interval);
            currentLevel = levels[currentLevelIndex];
            currentLevel.audio.play();
            interval = setInterval(DrawLoop, 1000 / 60);
        } 
    }
}



//  Need to replace currentLevel with currentLevel.
function DrawLoop() {
    //console.log("yo");
    //  Clear
    ClearCanvas(ctx);
    
    if(CheckEmpty(currentLevel)) {
        
        currentLevelIndex++;
        StartLevel();
        return;
    }
    DetectHit();
    CheckWallHit(currentLevel);
    ApplyTranslation(currentLevel);
    DetectBottom(levels[0   ]);

    //  draw level.

    //  draw ctx
    if(currentLevel != null) {
        moveToFalling(currentLevel);
        //console.log("boop");
        DrawLevel(currentLevel);
        ctx.drawImage(currentLevel.c,0,0);
        DrawPlayer(player);
        ctx.drawImage(player.c, player.x, HEIGHT - PLAYER_HEIGHT);
    }
    // check hits.

    // end
}

